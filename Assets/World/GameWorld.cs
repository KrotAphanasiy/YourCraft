using System.Collections.Generic;
using UnityEngine;
using World.Chunk;
using World.Constants;
using World.Generation;
using World.Generation.Terrain;

namespace World
{
    public class GameWorld : MonoBehaviour
    {
        public Dictionary<Vector2Int, ChunkData> Chunks = new();
        public ChunkRenderer ChunkPrefab;

        private Camera mainCamera;
    
        // Start is called before the first frame update
        private void Start()
        {
            mainCamera = Camera.main;
        
            for (int x = 0; x < 20; x++)
            {
                for (int y = 0; y < 20; y++)
                {
                    float xPos = x * WorldConstants.ChunkWidth * ChunkRenderer.BlockScale;
                    float zPos = y * WorldConstants.ChunkWidth * ChunkRenderer.BlockScale;
                
                    var chunkData = new ChunkData
                    {
                        Blocks = TerrainGenerator.GenerateTerrain(xPos, zPos),
                        ChunkPosition = new Vector2Int(x, y)
                    };

                    Chunks.Add(new Vector2Int(x, y), chunkData);
                
                    var chunk = Instantiate(ChunkPrefab, new Vector3(xPos, 0, zPos), Quaternion.identity, transform);
                    chunk.Chunk = chunkData;
                    chunk.ParentWorld = this;

                    chunkData.Renderer = chunk;
                }
            }
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
            {
                bool isDestroying = Input.GetMouseButtonDown(0);
            
                var ray = mainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

                if (Physics.Raycast(ray, out var hitInfo))
                {
                    Vector3 blockCenter;
                    if (isDestroying)
                    {
                        blockCenter = hitInfo.point - hitInfo.normal * ChunkRenderer.BlockScale / 2;
                    }
                    else
                    {
                        blockCenter = hitInfo.point + hitInfo.normal * ChunkRenderer.BlockScale / 2;
                    }

                    var blockWorldPos = Vector3Int.FloorToInt(blockCenter / ChunkRenderer.BlockScale);
                    var chunkPos = GetChunkContainingBlock(blockWorldPos);

                    if (Chunks.TryGetValue(chunkPos, out ChunkData chunkData))
                    {
                        var chunkOrigin = new Vector3Int(chunkPos.x, 0, chunkPos.y) * WorldConstants.ChunkWidth;
                        if (isDestroying)
                        {
                            chunkData.Renderer.DestroyBlock(blockWorldPos - chunkOrigin);
                        }
                        else
                        {
                            chunkData.Renderer.SpawnBlock(blockWorldPos - chunkOrigin);
                        }
                    }
                }
            }
        }

        public Vector2Int GetChunkContainingBlock(Vector3Int blockWorldPos)
        {
            return new Vector2Int(blockWorldPos.x / WorldConstants.ChunkWidth, blockWorldPos.z / WorldConstants.ChunkWidth);
        }
    }
}
