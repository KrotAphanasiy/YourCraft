using System;
using System.Collections.Generic;
using Enums;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using World.Constants;

namespace World.Chunk
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class ChunkRenderer : MonoBehaviour
    {
        public const float BlockScale = .25f;

        private Mesh _chunkMesh;
        private readonly List<Vector3> _vertexes = new();
        private readonly List<Vector2> _uvs = new();
        private readonly List<int> _triangles = new();

        public ChunkData Chunk;
        public GameWorld ParentWorld;
    
        // Start is called before the first frame update
        private void Start()
        {
            _chunkMesh = new Mesh();

            RegenerateMesh();

            GetComponent<MeshFilter>().mesh = _chunkMesh;
        }

        private void RegenerateMesh()
        {
            _vertexes.Clear();
            _uvs.Clear();
            _triangles.Clear();
        
            for (int y = 0; y < WorldConstants.ChunkHeight; y++)
            {
                for (int x = 0; x < WorldConstants.ChunkWidth; x++)
                {
                    for (int z = 0; z < WorldConstants.ChunkWidth; z++)
                    {
                        GenerateBlock(x, y, z);
                    }
                }
            }

            _chunkMesh.triangles = Array.Empty<int>();
            _chunkMesh.vertices = _vertexes.ToArray();
            _chunkMesh.uv = _uvs.ToArray();
            _chunkMesh.triangles = _triangles.ToArray();

            _chunkMesh.Optimize();
        
            _chunkMesh.RecalculateNormals();
            _chunkMesh.RecalculateBounds();
            GetComponent<MeshCollider>().sharedMesh = _chunkMesh;
        }

        private BlockType GetBlockAtPosition(Vector3Int blockPosition)
        {
            if (blockPosition.x >= 0 && blockPosition.x < WorldConstants.ChunkWidth
                                     && blockPosition.y >= 0 && blockPosition.y < WorldConstants.ChunkHeight
                                     && blockPosition.z >= 0 && blockPosition.z < WorldConstants.ChunkWidth)
            {
                return Chunk.Blocks[blockPosition.x, blockPosition.y, blockPosition.z];
            }
            else
            {
                if (blockPosition.y < 0 || blockPosition.y >= WorldConstants.ChunkHeight) return BlockType.Air;
            
                var adjacentChunkPosition = Chunk.ChunkPosition;

                if (blockPosition.x < 0)
                {
                    adjacentChunkPosition.x--;
                    blockPosition.x += WorldConstants.ChunkWidth;
                }
                if (blockPosition.x >= WorldConstants.ChunkWidth)
                {
                    adjacentChunkPosition.x++;
                    blockPosition.x -= WorldConstants.ChunkWidth;
                }
            
                if (blockPosition.z < 0)
                {
                    adjacentChunkPosition.y--;
                    blockPosition.z += WorldConstants.ChunkWidth;
                }
                if (blockPosition.z >= WorldConstants.ChunkWidth)
                {
                    adjacentChunkPosition.y++;
                    blockPosition.z -= WorldConstants.ChunkWidth;
                }

                if (ParentWorld.Chunks.TryGetValue(adjacentChunkPosition, out ChunkData adjacentChunk))
                {
                    return adjacentChunk.Blocks[blockPosition.x, blockPosition.y, blockPosition.z];
                }
                else
                {
                    return BlockType.Air;
                }
            }
        }

        public void SpawnBlock(Vector3Int blockPosition)
        {
            Chunk.Blocks[blockPosition.x, blockPosition.y, blockPosition.z] = BlockType.Stone;
            RegenerateMesh();
        }
    
        public void DestroyBlock(Vector3Int blockPosition)
        {
            Chunk.Blocks[blockPosition.x, blockPosition.y, blockPosition.z] = BlockType.Air;
            RegenerateMesh();
        }
    
        private void GenerateBlock(int x, int y, int z)
        {
            if (Chunk.Blocks[x, y, z] == BlockType.Air) return;

            var blockPosition = new Vector3Int(x, y, z);

            var blockType = GetBlockAtPosition(blockPosition);

            if (GetBlockAtPosition(blockPosition) == 0) return;

            if (GetBlockAtPosition(blockPosition + Vector3Int.right) == 0)
            {
                GenerateRightSide(blockPosition);
                AddUvs(blockType, Vector3Int.right);
            }

            if (GetBlockAtPosition(blockPosition + Vector3Int.left) == 0)
            {
                GenerateLeftSide(blockPosition);
                AddUvs(blockType, Vector3Int.left);
            }

            if (GetBlockAtPosition(blockPosition + Vector3Int.forward) == 0)
            {
                GenerateFrontSide(blockPosition);
                AddUvs(blockType, Vector3Int.forward);
            }

            if (GetBlockAtPosition(blockPosition + Vector3Int.back) == 0)
            {
                GenerateBackSide(blockPosition);
                AddUvs(blockType, Vector3Int.back);
            }

            if (GetBlockAtPosition(blockPosition + Vector3Int.up) == 0)
            {
                GenerateTopSide(blockPosition);
                AddUvs(blockType, Vector3Int.up);
            }

            if (GetBlockAtPosition(blockPosition + Vector3Int.down) == 0)
            {
                GenerateBottomSide(blockPosition);
                AddUvs(blockType, Vector3Int.down);
            }
        }

        private void GenerateLeftSide(Vector3Int blockPosition)
        {
            _vertexes.Add((new Vector3(0, 0, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(0, 0, 1) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(0, 1, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(0, 1, 1) + blockPosition) * BlockScale);

            AddLastVertexesSquare();
        }
    
        private void GenerateRightSide(Vector3Int blockPosition)
        {
            _vertexes.Add((new Vector3(1, 0, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 1, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 0, 1) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 1, 1) + blockPosition) * BlockScale);

            AddLastVertexesSquare();
        }
    
        private void GenerateBackSide(Vector3Int blockPosition)
        {
            _vertexes.Add((new Vector3(0, 0, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(0, 1, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 0, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 1, 0) + blockPosition) * BlockScale);

            AddLastVertexesSquare();
        }
    
        private void GenerateFrontSide(Vector3Int blockPosition)
        {
            _vertexes.Add((new Vector3(0, 0, 1) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 0, 1) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(0, 1, 1) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 1, 1) + blockPosition) * BlockScale);

            AddLastVertexesSquare();
        }
    
        private void GenerateTopSide(Vector3Int blockPosition)
        {
            _vertexes.Add((new Vector3(0, 1, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(0, 1, 1) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 1, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 1, 1) + blockPosition) * BlockScale);

            AddLastVertexesSquare();
        }
    
        private void GenerateBottomSide(Vector3Int blockPosition)
        {
            _vertexes.Add((new Vector3(0, 0, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 0, 0) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(0, 0, 1) + blockPosition) * BlockScale);
            _vertexes.Add((new Vector3(1, 0, 1) + blockPosition) * BlockScale);

            AddLastVertexesSquare();
        }

        private void AddLastVertexesSquare()
        {
            _triangles.Add(_vertexes.Count - 4);
            _triangles.Add(_vertexes.Count - 3);
            _triangles.Add(_vertexes.Count - 2);

            _triangles.Add(_vertexes.Count - 3);
            _triangles.Add(_vertexes.Count - 1);
            _triangles.Add(_vertexes.Count - 2);
        }

        private void AddUvs(BlockType blockType, Vector3Int normal)
        {
            Vector2 uv;

            if (blockType == BlockType.Grass)
            {
                if (normal == Vector3Int.up)
                {
                    uv = new Vector2(0f / 256, 240f / 256);
                }
                else if (normal == Vector3Int.down)
                {
                    uv = new Vector2(32f / 256, 240f / 256);
                }
                else if (normal == Vector3Int.left)
                {
                    uv = new Vector2(48f / 256, 240f / 256);
                }
                else if (normal == Vector3Int.right)
                {
                    uv = new Vector2(48f / 256, 240f / 256);
                }
                else if (normal == Vector3Int.forward)
                {
                    uv = new Vector2(48f / 256, 240f / 256);
                }
                else if (normal == Vector3Int.back)
                {
                    uv = new Vector2(48f / 256, 240f / 256);
                }
                else
                {
                    uv = new Vector2(160f / 256, 224f / 256);
                }
            }
            else if (blockType == BlockType.Stone)
            {
                uv = new Vector2(16f / 256, 240f / 256);
            }
            else
            {
                uv = new Vector2(160f / 256, 224f / 256);
            }

            for (int i = 0; i < 4; i++)
            {
                _uvs.Add(uv);
            }
        }
    }
}
