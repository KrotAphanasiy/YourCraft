using Enums;
using UnityEngine;

namespace World.Chunk
{
    public class ChunkData
    {
        public Vector2Int ChunkPosition;
        public ChunkRenderer Renderer;
        public BlockType[,,] Blocks;
    }
}