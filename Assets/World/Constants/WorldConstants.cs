namespace World.Constants
{
    public static class WorldConstants
    {
        public const int ChunkWidth = 16;
        public const int ChunkHeight = 128;
        public const int WaterLevel = 64;
    }
}