using Enums;

namespace World.Generation.Structure
{
    public class BlockToBuild
    {
        public readonly int X;
        public readonly int Y;
        public readonly int Z;

        public readonly BlockType BlockType;

        public BlockToBuild(int x, int y, int z, BlockType blockType)
        {
            X = x;
            Y = y;
            Z = z;
            BlockType = blockType;
        }
    }
}