using System.Collections.Generic;
using Enums;
using World.Chunk;

namespace World.Generation.Structure
{
    public class StructureBuilder
    {
        private List<BlockToBuild> _blocksToBuild;

        public void Build(ChunkData chunkData)
        {
            foreach (var block in _blocksToBuild)
            {
                chunkData.Blocks[block.X, block.Y, block.Z] = block.BlockType;
            }
        }

        public void MakeColumn(
            int x,
            int z,
            int yStart,
            int height,
            BlockType blockType)
        {
            
        }

        public void MakeRowX(
            int xStart, 
            int xEnd, 
            int y, 
            int z,
            BlockType blockType)
        {
            
        }
        
        public void MakeRowZ(
            int zStart, 
            int zEnd, 
            int x, 
            int y,
            BlockType blockType)
        {
            
        }

        public void Fill(
            int y, 
            int xStart, 
            int xEnd, 
            int zStart, 
            int zEnd,
            BlockType blockType)
        {
            
        }

        public void AddBlock(int x, int y, int z, BlockType blockType)
        {
            _blocksToBuild.Add(new BlockToBuild(x, y, z, blockType));
        }
    }
}