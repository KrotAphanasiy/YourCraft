using Enums;
using Maths;
using UnityEngine;
using World.Chunk;
using World.Constants;

namespace World.Generation.Terrain
{
    public static class TerrainGenerator
    {
        public static BlockType[,,] GenerateTerrain(float xOffset, float zOffset)
        {
            var result = new BlockType[WorldConstants.ChunkWidth, WorldConstants.ChunkHeight, WorldConstants.ChunkWidth];
            for (var x = 0; x < WorldConstants.ChunkWidth; x++)
            {
                for (int z = 0; z < WorldConstants.ChunkWidth; z++)
                {
                    var height = Mathf.PerlinNoise((x/4f + xOffset) * .2f, (z/4f + zOffset) * .2f) * 20 + 10;

                    for (int y = 0; y < height; y++)
                    {
                        result[x, y, z] = BlockType.Grass;
                    }
                }
            }

            return result;
        }
    }
}