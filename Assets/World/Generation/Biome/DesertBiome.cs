using Maths.Options;

namespace World.Generation.Biome
{
    public class DesertBiome : Biome
    {
        public DesertBiome(NoiseOptions noiseOptions, int treeFreq, int plantFreq, int seed) 
            : base(noiseOptions, treeFreq, plantFreq, seed)
        {
            
        }
    }
}