using Maths;
using Maths.Options;

namespace World.Generation.Biome
{
    public abstract class Biome
    {
        private NoiseGenerator _noiseGenerator;
        private int _treeFreq;
        private int _plantFreq;
        
        public Biome(NoiseOptions noiseOptions, int treeFreq, int plantFreq, int seed)
        {
            _noiseGenerator = new NoiseGenerator(seed);
            _treeFreq = treeFreq;
            _plantFreq = plantFreq;
        }
        
        
        
    }
}