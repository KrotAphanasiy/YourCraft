using Maths.Options;

namespace World.Generation.Biome
{
    public class OceanBiome : Biome
    {
        public OceanBiome(NoiseOptions noiseOptions, int treeFreq, int plantFreq, int seed) 
            : base(noiseOptions, treeFreq, plantFreq, seed)
        {
        }
    }
}