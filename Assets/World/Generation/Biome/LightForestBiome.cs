using Maths.Options;

namespace World.Generation.Biome
{
    public class LightForestBiome : Biome
    {
        public LightForestBiome(NoiseOptions noiseOptions, int treeFreq, int plantFreq, int seed) 
            : base(noiseOptions, treeFreq, plantFreq, seed)
        {
        }
    }
}