using Maths.Options;

namespace World.Generation.Biome
{
    public class GrasslandBiome : Biome
    {
        public GrasslandBiome(NoiseOptions noiseOptions, int treeFreq, int plantFreq, int seed) 
            : base(noiseOptions, treeFreq, plantFreq, seed)
        {
        }
    }
}