// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Bumped shader. Differences from regular Bumped one:
// - no Main Color
// - Normalmap uses Tiling/Offset of the Base texture
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "World Shader" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _TextureScale ("Texture Scale", float) = 1
        //[NoScaleOffset] _BumpMap ("Normalmap", 2D) = "bump" {}
    }

    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 250

        CGPROGRAM
        #pragma surface surf Lambert noforwardadd

        sampler2D _MainTex;
        float _TextureScale;

        struct Input {
            float2 uv_MainTex;
            float3 worldPos;
            float3 worldNormal;
        };

        void surf (Input IN, inout SurfaceOutput o) {
            const float x = IN.worldPos.x * _TextureScale;
            const float y = IN.worldPos.y * _TextureScale;
            const float z = IN.worldPos.z * _TextureScale;

            const float isUp = abs(IN.worldNormal.y);
            
            const float2 offset = float2(fmod(z + x * (1 - isUp), 0.0625), fmod(y + x * isUp, 0.0625));
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex + offset);
            o.Albedo = c.rgb;
            o.Alpha = 1;
        }
        ENDCG
    }

    FallBack "Mobile/Diffuse"
}