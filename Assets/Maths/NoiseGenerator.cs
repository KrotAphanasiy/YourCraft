using Maths.Options;
using World.Constants;
using System;

namespace Maths
{
    public class NoiseGenerator
    {
        private readonly int _seed;
        private NoiseOptions _noiseOptions;

        public NoiseGenerator(int seed)
        {
            _seed = seed;
        }

        public double GetHeight(int x, int z, int chunkX, int chunkZ)
        {
            var newX = (x + (chunkX * WorldConstants.ChunkWidth));
            var newZ = (z + (chunkZ * WorldConstants.ChunkWidth));

            if (newX < 0 || newZ < 0) {
                return WorldConstants.WaterLevel - 1;
            }

            var totalValue = 0.0;

            for (var a = 0; a < _noiseOptions.Octaves - 1; a++)
            {
                var frequency = Math.Pow(2.0, a);
                var amplitude = Math.Pow(_noiseOptions.Roughness, a);
                totalValue +=
                    Noise(newX * frequency / _noiseOptions.Smoothness,
                        newZ * frequency / _noiseOptions.Smoothness) *
                    amplitude;
            }

            var val = (((totalValue / 2.1) + 1.2) * _noiseOptions.Amplitude) +
                       _noiseOptions.HeightOffset;

            return val > 0 ? val : 1;
        }

        public void SetOptions(NoiseOptions noiseOptions)
        {
            _noiseOptions = noiseOptions;
        }

        private double GetNoise(int n)
        {
            n += _seed;
            n = (n << 13) ^ n;
            int newN = (n * (n * n * 60493 + 19990303) + 1376312589) & 0x7fffffff;

            return 1.0 - (newN / 1073741824.0);
        }
        
        private double GetNoise(double x, double z)
        {
            return GetNoise((int)(x + z * 57.0));
        }

        private double Lerp(double a, double b, double z)
        {
            var mu = (1 - Math.Cos(z * 3.14)) / 2;
            return (a * (1 - mu) + b * mu);
        }

        private double Noise(double x, double z)
        {
            var floorX = (double)((
                int)x);
            var floorZ = (double)((int)z);

            var s = 0.0;
            var t = 0.0;
            var u = 0.0;
            var v = 0.0;

            s = GetNoise(floorX, floorZ);
            t = GetNoise(floorX + 1, floorZ);
            u = GetNoise(
                floorX,
                floorZ + 1);
            v = GetNoise(floorX + 1, floorZ + 1);

            var rec1 = Lerp(s, t, x - floorX);
            var rec2 = Lerp(
                u, v,
                x - floorX);
            
            var rec3 =
                Lerp(rec1, rec2,
                    z - floorZ);
            return rec3;
        }
    }
}