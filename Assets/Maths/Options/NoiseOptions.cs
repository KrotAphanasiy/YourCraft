namespace Maths.Options
{
    public struct NoiseOptions
    {
        public readonly int Octaves;
        public readonly int Amplitude;
        public readonly int Smoothness;
        public readonly int HeightOffset;
        public readonly double Roughness;

        public NoiseOptions(int octaves, int amplitude, int smoothness, int heightOffset, double roughness)
        {
            Octaves = octaves;
            Amplitude = amplitude;
            Smoothness = smoothness;
            HeightOffset = heightOffset;
            Roughness = roughness;
        }
    };
}